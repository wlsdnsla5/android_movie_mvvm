package com.portfolio.movie_info_mvvm.util.movie

import com.portfolio.movie_info_mvvm.settings.MovieLog
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieClient {
    companion object {
        private var retrofit: Retrofit? = null

        fun getClient() = retrofit ?: HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }.let {
            val client = OkHttpClient.Builder().addInterceptor(it).build()
            retrofit = Retrofit.Builder()
                .baseUrl("https://utelly-tv-shows-and-movies-availability-v1.p.rapidapi.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            retrofit
        }

        fun searchByKeyword(keyword: String): Call<MovieCallResult>? {
            val service = getClient()?.create(MovieService::class.java) ?: return null
            val call = service.searchWihKeyword(keyword = keyword)
            return call.also {
                it.enqueue(object : Callback<MovieCallResult> {
                    override fun onResponse(
                        call: Call<MovieCallResult>,
                        response: Response<MovieCallResult>
                    ) {
                        MovieLog.putDebugLog("response - keyword \"$keyword\" : ${response.body()}")
                    }
                    override fun onFailure(call: Call<MovieCallResult>, t: Throwable) {
                        MovieLog.putErrorLog("Error during getting response - keyword \"$keyword\" : $t")
                    }
                })
            }
        }
    }
}