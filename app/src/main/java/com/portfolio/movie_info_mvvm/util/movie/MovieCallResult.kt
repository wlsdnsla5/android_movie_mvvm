package com.portfolio.movie_info_mvvm.util.movie

import com.google.gson.annotations.SerializedName

data class MovieCallResult(
    @SerializedName("result")
    val movieData: List<MovieData>
)

data class MovieData(
    @SerializedName("locations")
    val locations: List<RawMovie>,
    @SerializedName("weight")
    val weight: Long,
    @SerializedName("id")
    val id: String,
    @SerializedName("picture")
    val picture: String,
    @SerializedName("provider")
    val provider: String,
    @SerializedName("name")
    val name: String
)

data class RawMovie(
    @SerializedName("display_name")
    val displayName: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("icon")
    val icon: String
)