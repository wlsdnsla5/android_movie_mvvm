package com.portfolio.movie_info_mvvm.util.movie

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

/*
 Needs to be set for communication of Movie API by using okhttp, retrofit
  <uses-permission android:name="android.permission.INTERNET" />
  <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 currently using "Utelly" API to provide service from RAPID API
  {@link 'https://rapidapi.com/utelly/api/utelly/'}
*/
private const val HOST_ADDRESS = "utelly-tv-shows-and-movies-availability-v1.p.rapidapi.com"
private const val API_KEY = "b8fe958260mshfa960c5824b9969p12662ajsn66ea21fc8f69"

interface MovieService {
 @GET("/lookup?")
 fun searchWihKeyword(
  @Header("x-rapidapi-host") address: String = HOST_ADDRESS,
  @Header("x-rapidapi-key") key: String = API_KEY,
  @Query("term") keyword: String
 ): Call<MovieCallResult>
}