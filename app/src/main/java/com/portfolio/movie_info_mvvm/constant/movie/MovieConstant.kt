package com.portfolio.movie_info_mvvm.constant.movie

object MovieConstant {
    const val DATABASE_NAME = "movie_database"
    const val TABLE_MOVIE = "movie"
    const val TABLE_IMDB = "imdb"
    const val TABLE_TMDB = "tmdb"
    const val TABLE_WIKI = "wiki_data"
    const val TABLE_ACTOR = "actor"
    const val TABLE_DIRECTOR = "director"
    // COLUMN
    const val COLUMN_SCORE = "score"
    const val COLUMN_PROVIDER = "provider_id"
    const val COLUMN_ACTORS = "actors_id"
    const val COLUMN_TMDB = "tmdb_id"
    const val COLUMN_IMDB = "imdb_id"
    const val COLUMN_WIKI = "wiki_id"
    const val COLUMN_NAME = "name"
    const val COLUMN_DIRECTOR = "director_id"
    const val COLUMN_URL = "url"
    const val COLUMN_UPDATED = "updated_date"
}