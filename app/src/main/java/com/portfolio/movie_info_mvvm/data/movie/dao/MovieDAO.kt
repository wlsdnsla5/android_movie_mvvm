package com.portfolio.movie_info_mvvm.data.movie.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.portfolio.movie_info_mvvm.constant.movie.MovieConstant
import com.portfolio.movie_info_mvvm.data.movie.entity.Movie

const val FROM_MOVIE = "FROM ${MovieConstant.TABLE_MOVIE}"

@Dao
interface MovieDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addMovie(movie: Movie)

    @Query("select * $FROM_MOVIE")
    fun readAllMovie(): LiveData<List<Movie>>

    @Query("SELECT * $FROM_MOVIE $WHERE_NAME")
    fun findMovie(name: String): LiveData<List<Movie>>

    @Query("DELETE $FROM_MOVIE $WHERE_NAME")
    fun deleteMovieByName(name: String)
}
