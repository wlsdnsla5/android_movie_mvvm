package com.portfolio.movie_info_mvvm.data.movie.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.portfolio.movie_info_mvvm.data.movie.dao.FilmWorkerDAO
import com.portfolio.movie_info_mvvm.data.movie.dao.MovieDAO
import com.portfolio.movie_info_mvvm.data.movie.dao.ThirdPartyMovieDataDAO
import com.portfolio.movie_info_mvvm.data.movie.entity.*

open class MovieRepositoryImpl(
    private val movieDao: MovieDAO,
    private val filmWorkerDAO: FilmWorkerDAO,
    private val thirdPartyMovieDataDAO: ThirdPartyMovieDataDAO
) : MovieRepository {

    private var filmWorkerLiveData: MutableLiveData<ArrayList<FilmWorker>> =
        MutableLiveData(ArrayList())

    private var thirdPartyMovieDataLiveData: MutableLiveData<ArrayList<ThirdPartyMovieData>> =
        MutableLiveData(ArrayList())

    override fun findMovie(key: String) = movieDao.findMovie(key)

    override fun findFilmWorkerByName(key: String): MutableLiveData<ArrayList<FilmWorker>> {
        filmWorkerLiveData.value?.clear()
        filmWorkerLiveData.addDataToLiveData(findActor(key))
        filmWorkerLiveData.addDataToLiveData(findDirector(key))
        return filmWorkerLiveData
    }

    override fun findThirdPartyDataById(id: String): MutableLiveData<ArrayList<ThirdPartyMovieData>> {
        thirdPartyMovieDataLiveData.value?.clear()
        thirdPartyMovieDataLiveData.addDataToLiveData(findIMDB(id))
        thirdPartyMovieDataLiveData.addDataToLiveData(findTMDB(id))
        thirdPartyMovieDataLiveData.addDataToLiveData(findWIKI(id))
        return thirdPartyMovieDataLiveData
    }

    override fun getMovieFromHost(keyword: String) {
        // TODO get movie data from host and add the data to local DB
    }

    override suspend fun addMovie(movie: Movie) {
        movieDao.addMovie(movie)
    }

    override fun removeMovie(key: String) {
        movieDao.deleteMovieByName(key)
    }

    override fun readAllMovie() {

    }

    private fun findActor(key: String) = filmWorkerDAO.findActor(key)

    private fun findDirector(key: String) = filmWorkerDAO.findDirector(key)

    private fun findIMDB(key: String) = thirdPartyMovieDataDAO.findIMDB(key)

    private fun findTMDB(key: String) = thirdPartyMovieDataDAO.findTMDB(key)

    private fun findWIKI(key: String) = thirdPartyMovieDataDAO.findWIKI(key)

    private fun removeUnnecessaryData() {

    }

    @Synchronized
    private fun <T, X: T> MutableLiveData<ArrayList<T>>.addDataToLiveData(
        liveData: LiveData<List<X>>
    ) { liveData.map { value?.apply { this.addAll(it) } ?: ArrayList<T>(it) } }
}