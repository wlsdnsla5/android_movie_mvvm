package com.portfolio.movie_info_mvvm.data.movie.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.portfolio.movie_info_mvvm.constant.movie.MovieConstant
import kotlinx.android.parcel.Parcelize

sealed class FilmWorker : Parcelable {
    abstract val id: Int
    abstract val name: String
}

@Entity(tableName = MovieConstant.TABLE_DIRECTOR)
@Parcelize
data class Director(
    @PrimaryKey(autoGenerate = true)
    override val id: Int,

    @ColumnInfo(name = MovieConstant.COLUMN_NAME)
    override val name: String
) : FilmWorker()

@Entity(tableName = MovieConstant.TABLE_ACTOR)
@Parcelize
data class Actor(
    @PrimaryKey(autoGenerate = true)
    override val id: Int,

    @ColumnInfo(name = MovieConstant.COLUMN_NAME)
    override val name: String
) : FilmWorker()
