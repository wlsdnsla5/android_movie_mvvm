package com.portfolio.movie_info_mvvm.data.movie.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.portfolio.movie_info_mvvm.constant.movie.MovieConstant
import kotlinx.android.parcel.Parcelize

sealed class ThirdPartyMovieData : Parcelable {
    abstract val id: Int
    abstract val url: String
}

@Entity(tableName = MovieConstant.TABLE_IMDB)
@Parcelize
data class Imdb(
    @PrimaryKey(autoGenerate = true)
    override val id: Int,

    @ColumnInfo(name = MovieConstant.COLUMN_URL)
    override val url: String
) : ThirdPartyMovieData() {}

@Entity(tableName = MovieConstant.TABLE_TMDB)
@Parcelize
data class Tmdb(
    @PrimaryKey(autoGenerate = true)
    override val id: Int,

    @ColumnInfo(name = MovieConstant.COLUMN_URL)
    override val url: String
) : ThirdPartyMovieData() {}

@Entity(tableName = MovieConstant.TABLE_WIKI)
@Parcelize
data class Wiki(
    @PrimaryKey(autoGenerate = true)
    override val id: Int,

    @ColumnInfo(name = MovieConstant.COLUMN_URL)
    override val url: String
) : ThirdPartyMovieData() {}