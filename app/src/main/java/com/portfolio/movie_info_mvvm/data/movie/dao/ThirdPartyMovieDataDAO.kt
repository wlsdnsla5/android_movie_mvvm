package com.portfolio.movie_info_mvvm.data.movie.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.portfolio.movie_info_mvvm.constant.movie.MovieConstant
import com.portfolio.movie_info_mvvm.data.movie.entity.Imdb
import com.portfolio.movie_info_mvvm.data.movie.entity.Tmdb
import com.portfolio.movie_info_mvvm.data.movie.entity.Wiki

const val FROM_IMDB = "FROM ${MovieConstant.TABLE_IMDB}"
const val FROM_TMDB = "FROM ${MovieConstant.TABLE_TMDB}"
const val FROM_WIKI = "FROM ${MovieConstant.TABLE_WIKI}"
const val WHERE_URL = "WHERE ${MovieConstant.COLUMN_URL} LIKE '%' || :url || '%'"

@Dao
interface ThirdPartyMovieDataDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addIMDB(imdb: Imdb)

    @Query("SELECT * $FROM_IMDB")
    fun readAllIMDB(): LiveData<List<Imdb>>

    @Query("SELECT * $FROM_IMDB $WHERE_URL")
    fun findIMDB(url: String): LiveData<List<Imdb>>

    @Query("DELETE $FROM_IMDB $WHERE_URL")
    fun deleteIMDBByName(url: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTMDB(tmdb: Tmdb)

    @Query("select * $FROM_TMDB")
    fun readAllTMDB(): LiveData<List<Tmdb>>

    @Query("select * $FROM_TMDB $WHERE_URL")
    fun findTMDB(url: String): LiveData<List<Tmdb>>

    @Query("DELETE $FROM_TMDB $WHERE_URL")
    fun deleteTMDBByName(url: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addWIKI(wiki: Wiki)

    @Query("select * $FROM_WIKI")
    fun readAllWIKI(): LiveData<List<Wiki>>

    @Query("select * $FROM_WIKI $WHERE_URL")
    fun findWIKI(url: String): LiveData<List<Wiki>>

    @Query("DELETE $FROM_WIKI $WHERE_URL")
    fun deleteWIKIByName(url: String)
}