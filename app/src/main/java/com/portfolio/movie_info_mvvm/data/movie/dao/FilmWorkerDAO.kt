package com.portfolio.movie_info_mvvm.data.movie.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.portfolio.movie_info_mvvm.constant.movie.MovieConstant
import com.portfolio.movie_info_mvvm.data.movie.entity.Actor
import com.portfolio.movie_info_mvvm.data.movie.entity.Director

const val FROM_ACTOR = "FROM ${MovieConstant.TABLE_DIRECTOR}"
const val FROM_DIRECTOR = "FROM ${MovieConstant.TABLE_DIRECTOR}"
const val WHERE_NAME = "WHERE ${MovieConstant.COLUMN_NAME} LIKE '%' || :name || '%'"

@Dao
interface FilmWorkerDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addActor(actor: Actor)

    @Query("select * $FROM_ACTOR")
    fun readAllActor(): LiveData<List<Actor>>

    @Query("SELECT * $FROM_ACTOR $WHERE_NAME")
    fun findActor(name: String): LiveData<List<Actor>>

    @Query("DELETE $FROM_ACTOR $WHERE_NAME")
    fun deleteActorByName(name: String)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addDirector(director: Director)

    @Query("select * from ${MovieConstant.TABLE_DIRECTOR}")
    fun readAllDirector(): LiveData<List<Actor>>

    @Query("SELECT * $FROM_DIRECTOR $WHERE_NAME")
    fun findDirector(name: String): LiveData<List<Director>>

    @Query("DELETE $FROM_DIRECTOR $WHERE_NAME")
    fun deleteDirectorByName(name: String)
}