package com.portfolio.movie_info_mvvm.data.movie

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.portfolio.movie_info_mvvm.constant.movie.MovieConstant
import com.portfolio.movie_info_mvvm.data.movie.dao.FilmWorkerDAO
import com.portfolio.movie_info_mvvm.data.movie.dao.MovieDAO
import com.portfolio.movie_info_mvvm.data.movie.dao.ThirdPartyMovieDataDAO
import com.portfolio.movie_info_mvvm.data.movie.entity.*

@Database(entities = [Movie::class, Wiki::class, Actor::class, Director::class, Imdb::class, Tmdb::class], version = 1, exportSchema = true)
abstract class MovieDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDAO
    abstract fun fileWorkerDao(): FilmWorkerDAO
    abstract fun thirdPartyMovieDataDAO(): ThirdPartyMovieDataDAO

    companion object {
        @Volatile
        private var INSTANCE: MovieDatabase? = null

        fun getDatabase(context: Context): MovieDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MovieDatabase::class.java,
                    MovieConstant.DATABASE_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }

    }
}