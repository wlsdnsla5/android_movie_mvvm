package com.portfolio.movie_info_mvvm.data.movie.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.portfolio.movie_info_mvvm.constant.movie.MovieConstant
import kotlinx.android.parcel.Parcelize

@Entity(tableName = MovieConstant.TABLE_MOVIE)
@Parcelize
data class Movie(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = MovieConstant.COLUMN_NAME)
    val name: String,

    @ColumnInfo(name = MovieConstant.COLUMN_SCORE)
    val score: Float,

    @ColumnInfo(name = MovieConstant.COLUMN_PROVIDER)
    val provider: String,

    @ColumnInfo(name = MovieConstant.COLUMN_IMDB)
    val imdb: String,

    @ColumnInfo(name = MovieConstant.COLUMN_TMDB)
    val tmdb: String,

    @ColumnInfo(name = MovieConstant.COLUMN_DIRECTOR)
    val director: String,

    @ColumnInfo(name = MovieConstant.COLUMN_ACTORS)
    val actors: String,

    @ColumnInfo(name = MovieConstant.COLUMN_WIKI)
    val wiki: String,

    @ColumnInfo(name = MovieConstant.COLUMN_UPDATED)
    val updated: Long
) : Parcelable