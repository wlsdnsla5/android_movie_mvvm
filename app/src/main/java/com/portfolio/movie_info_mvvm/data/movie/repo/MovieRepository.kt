package com.portfolio.movie_info_mvvm.data.movie.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.portfolio.movie_info_mvvm.data.movie.entity.*

interface MovieRepository {
    /*
        This repository should pass data for View to VM.

        Main functionality of Repo

        1. manage the cache storage
        2. data source of View
    */

    fun findMovie(key: String): LiveData<List<Movie>>
    fun findFilmWorkerByName(key: String): MutableLiveData<ArrayList<FilmWorker>>
    fun findThirdPartyDataById(id: String): MutableLiveData<ArrayList<ThirdPartyMovieData>>
    fun getMovieFromHost(keyword: String)
    suspend fun addMovie(movie: Movie)
    fun removeMovie(key: String)
    fun readAllMovie()
}