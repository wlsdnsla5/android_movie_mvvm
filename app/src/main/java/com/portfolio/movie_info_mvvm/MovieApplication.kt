package com.portfolio.movie_info_mvvm

import android.app.Application
import com.portfolio.movie_info_mvvm.data.movie.repo.MovieRepository
import com.portfolio.movie_info_mvvm.data.movie.repo.MovieRepositoryImpl
import org.koin.android.ext.android.startKoin
import org.koin.dsl.module.module

class MovieApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        val appModule = module {
            single { MovieRepositoryImpl(get(), get(), get()) as MovieRepository }
        }
        startKoin (applicationContext, listOf(appModule))
    }
}