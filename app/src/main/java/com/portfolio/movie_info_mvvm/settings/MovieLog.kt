package com.portfolio.movie_info_mvvm.settings

import android.util.Log

private const val DEBUG = "DEBUG"
private const val ERORR = "ERROR"
private const val TEST_DEBUG = "TEST"

object MovieLog {
    fun putErrorLog(msg: String) = Log.e(ERORR, msg)
    fun putDebugLog(msg: String) = Log.d(DEBUG, msg)
    fun putTestDebugLog(msg: String) = Log.i(TEST_DEBUG, msg)
}